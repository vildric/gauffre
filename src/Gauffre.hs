{-# LANGUAGE OverloadedStrings #-}
module Main where

import Data.Text (Text, pack)
import Data.Text.Encoding (encodeUtf8)

import Control.Applicative
import Control.Monad.IO.Class (liftIO)

import Database.Persist.Sqlite

import Snap.Core
import Snap.Http.Server
import Snap.Util.FileServe

import System.Environment

import Gauffre.Types
import Gauffre.Handler
import Gauffre.Handler.User (login, logout, register)
import Gauffre.Handler.Page (viewPage)
import Gauffre.Handler.Discussion (editMessage)
import Gauffre.Persistence.Discussion (newDiscussion)
import Gauffre.Routes

recaptcha = RecaptchaConf {
    recaptchaPubKey = "6LdbfM4SAAAAAKWsY74kHT_dKe3jtMnBwkA1tEvn"
  , recaptchaPrivKey = "6LdbfM4SAAAAALaV4X5du9YklAXWoiOa405JCwot" }

routeHandler = do
  r <- getRoute
  case r of
    Just r' -> do
      hs <- liftIO $ withDb $ \c -> return $ HandlerState {
                                               getConnection = c                                                                                       , recaptchaConf = recaptcha
                                             , currentRoute = r' }
      runHandler (handle r') hs
    Nothing -> empty

handle :: Route -> Handler ()
handle Login = login
handle Logout = logout
handle (EditMessage mid) = editMessage mid
handle Register = register
handle (ViewPage path) = viewPage path

serve = routeHandler
        <|> dir "static" (serveDirectory "static")

dbName = "gauffre.db"

withDb = withSqlitePool dbName 1

server :: IO ()
server = httpServe defaultConfig serve

withConn = withSqliteConn "gauffre.db" . runSqlConn

migrateDb :: IO ()
migrateDb =  withConn $ runMigration migrateAll

run ["migrer"] = migrateDb
run ["nouvelle_discussion", p] = withConn $ newDiscussion p >> return ()
run [] = server

main :: IO ()
main = run =<< getArgs
