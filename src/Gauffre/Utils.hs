{-# LANGUAGE OverloadedStrings #-}
module Gauffre.Utils where

import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import qualified Data.Text.Lazy.Encoding as LE
import Data.Text.Read
import qualified Data.Digest.Pure.SHA as S

import Text.Pandoc
import Text.Blaze (Html)

import Control.Applicative

import System.Random (randomRIO)

genToken :: IO Text
genToken = T.pack <$> go 128
  where go 0 = pure []
        go n = (:) <$> randomRIO ('a', 'z') <*> go (n - 1)

sha256 :: Text -> Text
sha256 = T.pack . S.showDigest . S.sha256 . LE.encodeUtf8 . L.fromStrict

markdown :: Text -> Html
markdown = writeHtml defaultWriterOptions
           . readMarkdown defaultParserState
           . T.unpack

readMay :: Reader a -> Text -> Maybe a
readMay rdr t = case rdr t of
  Left err -> Nothing
  Right (v, r) -> if T.null r then Just v else Nothing

readIntMay :: Text -> Maybe Int
readIntMay = readMay decimal

maybeAlt :: (Alternative t) => Maybe a -> t a
maybeAlt Nothing = empty
maybeAlt (Just a) = pure a

toPath :: [Text] -> Text
toPath = T.intercalate "/"
