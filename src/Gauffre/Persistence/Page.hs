module Gauffre.Persistence.Page where

import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO

import Control.Applicative

import System.FilePath
import System.Directory
import System.IO

import Gauffre.Types

skipSpaces :: Text -> Text
skipSpaces = T.dropWhile (== ' ')

readConfLine :: Text -> (Maybe Text, Text)
readConfLine s = case T.uncons s of
  Just ('%', r) -> let (x, y) = T.break (== '\n') r in
                   let x' = skipSpaces x in
                   if T.null y then (Just x, T.empty) else (Just x, T.tail y)
  Just (x, xs) -> (Nothing, T.cons x xs)
  Nothing -> (Nothing, T.empty)

readParam :: Text -> (Text, Maybe Text)
readParam s = let (x, y) = T.break (== ':') s in
              let x' = T.strip x in
              if T.null y
                  then (x', Nothing)
                  else (x', Just . T.strip $ T.tail y)

parsePage :: Text -> Page
parsePage s = let (attrs, text) = loop (readConfLine s)
              in Page { pageText = skipNL text
                     , pageAttrs = attrs }
    where loop (Nothing, r) = ([], r)
          loop (Just line, r) = let (attr, r') = loop $ readConfLine r in
                                ((readParam line):attr, r')
          skipNL = T.dropWhile (== '\n')

pageDir = "pages"
getPage :: FilePath -> IO (Maybe Page)
getPage filepath = do
  let path = pageDir </> filepath <.> "page"
  e <- doesFileExist path
  if e then (Just . parsePage) <$> TIO.readFile path else return Nothing
