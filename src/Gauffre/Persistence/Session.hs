module Gauffre.Persistence.Session where

import Data.Text
import Data.Time.Clock

import Control.Monad.IO.Class (liftIO)

import Database.Persist

import Gauffre.Types
import Gauffre.Utils

getSession :: Text -> Persistence (Maybe (Entity Session))
getSession token = selectFirst [SessionToken ==. token] []

newSession :: UserId -> UTCTime -> Persistence Text
newSession uId expiresAt = do
  token <- liftIO genToken
  insert $ Session token uId expiresAt
  return token

deleteOldSessions :: Persistence ()
deleteOldSessions = do
  now <- liftIO getCurrentTime
  deleteWhere [SessionExpiresAt <. now]

deleteSession :: Text -> Persistence ()
deleteSession token = deleteWhere [SessionToken ==. token]
