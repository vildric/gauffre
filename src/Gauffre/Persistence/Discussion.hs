{-# LANGUAGE OverloadedStrings #-}
module Gauffre.Persistence.Discussion where

import Data.Text
import Data.Time.Clock
import Data.Monoid

import Control.Monad.IO.Class (liftIO)

import Database.Persist
import Database.Persist.GenericSql

import Gauffre.Types

getDiscussionFor :: FilePath -> Persistence (Maybe (Entity Discussion))
getDiscussionFor path = selectFirst [DiscussionAttachedTo ==. path] []

getMessages :: DiscussionId -> Persistence [(Entity Message, Entity User)]
getMessages disc = rawSql (mconcat query) [toPersistValue disc]
    where query = [ "SELECT ??, ?? "
                  , "FROM \"Message\" "
                  , "LEFT JOIN \"User\" "
                  , "ON \"Message\".author = \"User\".id "
                  , "WHERE \"Message\".discussion = ? "
                  , "ORDER BY \"Message\".\"postedAt\"" ]

newMessage :: Text -> UserId -> DiscussionId -> Persistence MessageId
newMessage body author disc = do
  now <- liftIO getCurrentTime
  insert $ Message author disc body now

updateMessageBody :: MessageId -> Text -> Persistence ()
updateMessageBody mId newBody = update mId [MessageBody =. newBody]

newDiscussion :: FilePath -> Persistence DiscussionId
newDiscussion attachedTo = do
  now <- liftIO getCurrentTime
  insert $ Discussion now attachedTo

