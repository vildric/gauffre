module Gauffre.Persistence.User where

import Data.Text

import Database.Persist

import Gauffre.Types
import Gauffre.Utils

newUser :: Text -> Text -> Text -> Persistence UserId
newUser name password email = insert $ User name (sha256 password) email

findUser :: Text -> Text -> Persistence (Maybe (Entity User))
findUser name password = selectFirst [UserName ==. name
                                , UserPassword ==. sha256 password] []
