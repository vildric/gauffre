{-# LANGUAGE OverloadedStrings #-}
module Gauffre.Auth where

import Data.Time.Clock
import Data.Time.Calendar
import Data.Text.Encoding (encodeUtf8, decodeUtf8)

import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Maybe

import Control.Applicative

import Database.Persist

import Snap.Core

import Gauffre.Types
import Gauffre.Persistence.Session
import Gauffre.Utils
import Gauffre.Routes
import Gauffre.Handler

sessionCookieName = "gauffre_session_id"

addSessionCookie :: UserId -> Handler ()
addSessionCookie uid = do
  now <- liftIO getCurrentTime
  let day = utctDay now
  let expiresAtDay = addDays 8 day
  let expiresAt = now { utctDay = expiresAtDay }
  tok <- runDb $ newSession uid expiresAt
  let cookie = Cookie {
      cookieName = sessionCookieName
    , cookieValue = encodeUtf8 tok
    , cookieExpires = Just expiresAt
    , cookieDomain = Nothing
    , cookiePath = Nothing
    , cookieSecure = False
    , cookieHttpOnly = True }
  modifyResponse (addResponseCookie cookie)

getLoggedInUser :: Handler (Maybe (Entity User))
getLoggedInUser = runMaybeT $ do
  cookie <- MaybeT $ getCookie sessionCookieName
  Entity _ session <- MaybeT . runDb
                        . getSession . decodeUtf8 $ cookieValue cookie
  let uid = sessionUser session
  Entity uid <$> (MaybeT . runDb $ get uid)

loginRequired :: (Entity User -> Handler a) -> Handler a
loginRequired m = do
  mu <- getLoggedInUser
  case mu of
    Just u -> m u
    Nothing -> redirectTo Login
