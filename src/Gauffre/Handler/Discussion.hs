{-# LANGUAGE OverloadedStrings #-}
module Gauffre.Handler.Discussion where

import Data.Text.Encoding (encodeUtf8, decodeUtf8)
import Text.Digestive
import Text.Digestive.Blaze.Html5
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
import qualified Data.Text as T

import Control.Applicative
import Control.Monad.Reader

import Database.Persist

import Snap.Core

import Gauffre.Types
import Gauffre.Handler
import Gauffre.Auth
import Gauffre.Layout
import Gauffre.Discussion
import Gauffre.Persistence.Discussion
import Gauffre.Routes

editMessage :: MessageId -> Handler ()
editMessage msgId = loginRequired $ \(Entity uid _) -> do
  msg <- runDb $ get msgId
  case msg of
    Just msg' -> do
      if uid /= messageAuthor msg'
          then
            messageBox ("Vous ne pouvez pas modifier un message dont vous "
              `T.append` "n'êtes pas l'auteur.") Failure
          else do
      next' <- getParam "suivant"
      let next = maybe (ViewPage "/") (ViewPage . decodeUtf8) next'
      r <- handleForm (messageForm next . Just $ messageBody msg') "message"
      case r of
        (_, Just (body, next)) -> do
          runDb $ updateMessageBody msgId body
          redirect $ encodeUtf8 next
        (formHtml, Nothing) -> layoutPage "Modification d'un message" $ do
          formHtml
    Nothing -> empty
