{-# LANGUAGE OverloadedStrings #-}
module Gauffre.Handler.User where

import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)

import Text.Digestive
import Text.Digestive.Blaze.Html5
import Text.Digestive.Forms.Snap

import Snap.Core

import Control.Applicative

import Database.Persist

import Gauffre.Types
import Gauffre.Handler
import Gauffre.Auth
import Gauffre.Layout
import Gauffre.Persistence.User (findUser, newUser)
import Gauffre.Persistence.Session (deleteSession)
import Gauffre.Recaptcha
import Gauffre.Routes

loginForm = (,) <$> (label "Nom d'utilisateur" ++> inputText Nothing)
                <*> (label "Mot de passe" ++> inputPassword False)

doLogin = do
  r <- handleForm loginForm "connexion"
  case r of
    (_, Just (u, p)) -> do
      musr <- runDb $ findUser u p
      case musr of
        Just (Entity uid _) -> addSessionCookie uid
                  >> messageBox "Vous êtes maintenant connecté." Notice
        Nothing -> messageBox "Échec de la connexion." Failure
    (formHtml, Nothing) -> layoutPage "Connexion" formHtml

login :: Handler ()
login = do
  mu <- getLoggedInUser
  case mu of
    Just (Entity _ user) -> messageBox ("Vous êtes déjà connecté, "
                              `T.append` userName user) Notice
    Nothing -> doLogin

logout :: Handler ()
logout = do
  cook <- getCookie sessionCookieName
  case cook of
    Just tok -> expireCookie sessionCookieName Nothing
                >> runDb (deleteSession . decodeUtf8 $ cookieValue tok)
                >> messageBox "Vous êtes bien déconnecté." Notice
    Nothing -> redirectHome

data RegisterForm = RegisterForm Text Text Text Text

registerForm = RegisterForm <$> (label "Nom d'utilisateur" ++> inputText Nothing)
                            <*> (label "Mot de passe" ++> inputPassword False)
                            <*> (label "Confirmation du mot de passe" ++>
                                  inputPassword False)
                            <*> (label "Adresse électronique" ++>
                                  inputText Nothing)
register :: Handler ()
register = do
  let disp formHtml = do {
    captcha <- displayCaptcha;
    layoutPage "Inscription" (displayForm formHtml captcha) }
  form <- runViewSnapForm registerForm "inscription"
  case form of
    (formHtml, Nothing) -> disp formHtml
    (formHtml, Just (RegisterForm name pwd confirm email)) -> do
      cap <- handleCaptcha
      if cap && pwd == confirm
          then runDb (newUser name pwd email)
               >> messageBox "Bienvenue dans la cabale." Notice
          else disp formHtml

