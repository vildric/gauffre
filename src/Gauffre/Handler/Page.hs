{-# LANGUAGE OverloadedStrings #-}
module Gauffre.Handler.Page where

import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)

import Control.Applicative
import Control.Monad.IO.Class
import Control.Monad.Reader

import Snap.Core

import Database.Persist

import Gauffre.Types
import Gauffre.Handler
import Gauffre.Layout
import Gauffre.Utils
import Gauffre.Persistence.Page (getPage)
import Gauffre.Persistence.Discussion
import Gauffre.Discussion

viewPage' :: FilePath -> Handler ()
viewPage' path = do
  pg <- liftIO $ getPage path
  case pg of
    Just page -> do
      let pageTitle = case lookup "titre" (pageAttrs page) of
                        Just (Just t) -> t
                        _ -> T.pack path
      disc <- runDb $ getDiscussionFor path
      r <- asks currentRoute
      dhtml <- case disc of
        Just (Entity discId _) -> discussion r discId
        Nothing -> return $ text "jes"
      layoutPage pageTitle $ do
        markdown . pageText $ page
        dhtml
    Nothing -> empty

goodPath :: FilePath -> Bool
goodPath = all (`elem` chars)
    where chars = ['a'..'z'] ++ "éèêâàûù/-_" ++ ['1'..'9']

viewPage :: Text -> Handler ()
viewPage pathText = do
  let path = T.unpack pathText
  if goodPath path
      then viewPage' path
      else empty
