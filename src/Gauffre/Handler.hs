{-# LANGUAGE OverloadedStrings #-}
module Gauffre.Handler where

import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8, decodeUtf8)
import Data.List

import Control.Applicative
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Reader
import Control.Arrow

import Snap.Core

import Text.Digestive.Forms.Snap
import Text.Digestive.Blaze.Html5
import Text.Digestive.Types

import Text.Blaze ((!), toHtml, toValue, Html)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
import Text.Blaze.Renderer.Text (renderHtml)

import Database.Persist.GenericSql
import Database.Persist.Store

import Gauffre.Types
import Gauffre.Utils
import Gauffre.Recaptcha

runDb :: Persistence a -> Handler a
runDb p = do
  conn <- asks getConnection
  liftIO $ runSqlPool p conn

runHandler = runReaderT . unHandler

displayForm :: FormHtml Html -> Html -> Html
displayForm form extra = do
  let (formHtml, encType) = renderFormHtml form
  H.form ! A.enctype (toValue $ show encType)
         ! A.method "POST" ! A.action "" $ do
    formHtml
    extra
    H.input ! A.type_ "submit" ! A.value "Envoyer"

handleForm :: (MonadSnap m) =>
               SnapForm m e (FormHtml Html) a -> String -> m (Html, Maybe a)
handleForm form slug = do
  method <- getsRequest rqMethod
  case method of
    GET -> do
      form <- viewForm form slug
      return (flip displayForm (return ()) form, Nothing)
    _ -> first (flip displayForm (return ())) <$> runViewSnapForm form slug


