{-# LANGUAGE OverloadedStrings #-}
module Gauffre.Recaptcha where

import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8, decodeUtf8)
import Data.ByteString (ByteString)
import Data.Enumerator
import Data.Enumerator.Text as ET
import Data.Monoid

import Text.Blaze (Html, (!), toValue)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import Control.Applicative
import Control.Monad.IO.Class (liftIO)
import Control.Arrow (second)
import Control.Monad.Reader

import Network.HTTP.Enumerator

import Snap.Core

import Gauffre.Types

apiUrl = "http://www.google.com/recaptcha/api/verify"

captchaSuccess :: (Monad m) => Iteratee ByteString m Bool
captchaSuccess = fmap isTrue $ decode utf8 =$ ET.takeWhile (/= '\n')
    where isTrue = (== "true")

validateCaptcha :: Text -> Text -> Text -> Text -> IO Bool
validateCaptcha privKey remoteip chlng resp = do
  req <- parseUrl apiUrl
  let params = [ ("privatekey", privKey)
               , ("remoteip", remoteip)
               , ("challenge", chlng)
               , ("response", resp) ]
  let req2 = urlEncodedBody (fmap (second encodeUtf8) params) req
  withManager $ run_ . http req2 (\_ _ -> captchaSuccess)

handleCaptcha :: Handler Bool
handleCaptcha = do
  chlng <- fmap decodeUtf8 <$> getParam "recaptcha_challenge_field"
  resp <- fmap decodeUtf8 <$> getParam "recaptcha_response_field"
  privKey <- asks (recaptchaPrivKey . recaptchaConf)
  ipHeaderFilter
  ip <- decodeUtf8 <$> getsRequest rqRemoteAddr
  case (chlng, resp) of
    (Just chlng', Just resp') -> liftIO $ validateCaptcha privKey ip chlng' resp'
    _ -> (liftIO $ putStrLn "foo") >> return False

displayCaptcha :: Handler Html
displayCaptcha =  asks (recaptchaPubKey . recaptchaConf) >>= \key -> return $ do
  H.script ! A.type_ "text/javascript"
           ! A.src (toValue $ "http://www.google.com/recaptcha/api/"
                     `T.append` "challenge?k=" `T.append` key) $ return ()
  H.noscript $ do
     H.iframe ! A.src (toValue $ "http://www.google.com/recaptcha/api/noscript"
                        `T.append` "?k=" `T.append` key)
              ! A.height "300" ! A.width "500" $ do
       H.br
       H.textarea ! A.name "recaptcha_challenge_field" ! A.rows "3" ! A.cols "40"
         $ return ()
       H.input ! A.type_ "hidden" ! A.name "recaptcha_response_field"
               ! A.value "manual_challenge"
