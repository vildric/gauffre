{-# LANGUAGE OverloadedStrings #-}
module Gauffre.Discussion where

import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8)
import Data.Maybe
import Text.Blaze (Html, toHtml, toValue, (!))
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
import Text.Digestive.Blaze.Html5 (inputTextArea, inputHidden)
import Text.Digestive.Forms.Snap

import Control.Applicative
import Control.Monad (forM_)

import System.FilePath

import Snap.Core

import Database.Persist

import Gauffre.Types
import Gauffre.Handler
import Gauffre.Auth
import Gauffre.Layout
import Gauffre.Utils
import Gauffre.Routes
import Gauffre.Persistence.Discussion

renderMessage :: Route -> Entity Message -> Entity User -> Html
renderMessage r (Entity msgId msg) (Entity _ author) = do
  let next = urlFor (EditMessage msgId)
             `T.append` "?suivant="
             `T.append` T.tail (urlFor r)
  H.strong . text $ userName author
  H.br
  H.p . markdown $ messageBody msg
  H.a ! A.href (toValue next) $ "modifier"
  H.hr

renderDiscussion :: Route -> [(Entity Message, Entity User)] -> Html
renderDiscussion r msgs = do
  H.h2 "Discussion"
  H.div ! A.id "discussion" $ forM_ msgs (uncurry $ renderMessage r)

messageForm next def = (,) <$> inputTextArea (Just 20) (Just 80) def
                           <*> inputHidden (Just $ urlFor next)

discussion :: Route -> DiscussionId -> Handler Html
discussion next disc = do
  r <- handleForm (messageForm next Nothing) "message"
  html <- case r of
    (formHtml, Just (body, next)) -> loginRequired $ \(Entity uid _) -> do
      runDb $ newMessage body uid disc
      redirect $ encodeUtf8 next
    (formHtml, Nothing) -> return formHtml
  msgs <- runDb $ getMessages disc
  mu <- getLoggedInUser
  return $ do
    renderDiscussion next msgs
    if isJust mu
      then do
        H.h3 "Écrire un message"
        html
      else return ()
