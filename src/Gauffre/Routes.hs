{-# LANGUAGE OverloadedStrings, NoMonomorphismRestriction #-}
module Gauffre.Routes where

import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8, decodeUtf8)

import Control.Applicative

import Database.Persist
import Database.Persist.Store

import Snap.Core (MonadSnap, getsRequest, rqPathInfo, redirect)

import Gauffre.Types
import Gauffre.Utils

pathSegments :: (MonadSnap m) => m [Text]
pathSegments = (T.splitOn "/" . decodeUtf8) <$> getsRequest rqPathInfo

redirectTo :: (MonadSnap m) => Route -> m b
redirectTo = redirect . encodeUtf8 . urlFor

redirectHome :: (MonadSnap m) => m a
redirectHome = redirect "/"

getRoute = route <$> pathSegments

route ["connexion"] = pure Login
route ["deconnexion"] = pure Logout
route ["inscription"] = pure Register
route ["message", mid, "modifier"] = EditMessage <$> fromText mid
route segs = pure $ ViewPage (toPath segs)

toUrl Login = ["connexion"]
toUrl Logout = ["deconnexion"]
toUrl Register = ["inscription"]
toUrl (EditMessage mid) = ["message", toText mid, "modifier"]
toUrl (ViewPage path) = [path]

urlFor = toPath . ("":) . toUrl

instance Param Text where
  toText = id
  fromText = Just

instance Param Int where
  toText = T.pack . show
  fromText = readIntMay

instance Param (Key b e) where
  toText (Key (PersistInt64 i)) = T.pack . show $ fromIntegral i
  toText _ = "invalid key"
  fromText x = fmap (Key . PersistInt64 . fromIntegral) (fromText x :: Maybe Int)

