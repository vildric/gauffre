{-# LANGUAGE OverloadedStrings #-}
module Gauffre.Layout where

import Data.Text (Text)

import Control.Monad (forM_)

import Text.Blaze ((!), toHtml, toValue, Html)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A
import Text.Blaze.Renderer.Text (renderHtml)

import Snap.Core

import Gauffre.Types
import Gauffre.Auth
import Gauffre.Routes
import Gauffre.Handler

writeHtml :: Html -> Handler ()
writeHtml html = do
  modifyResponse (addHeader "Content-Type" "text/html")
  writeLazyText $ renderHtml html

userBar Nothing = H.li (H.a ! A.href
                                (toValue $ urlFor Login)  $ "Connexion")
userBar (Just _) = do
  navBar [("Profil", "#"), ("Déconnexion", urlFor Logout)]

navBar :: [(Text, Text)] -> Html
navBar links = forM_ links
                $ \(name, url) -> H.li (H.a ! A.href (toValue url) $ text name)

menu :: [(Text, Text)]
menu = [ ("Accueil", "/")
       , ("Forum", "#")
       , ("Articles", "#") ]

layoutPage :: Text -> Html -> Handler ()
layoutPage title body = do
  user <- getLoggedInUser
  writeHtml . H.docTypeHtml $ do
  H.head $ do
    H.title $ toHtml title
    H.meta ! A.charset "utf-8"
    H.link ! A.type_ "text/css" ! A.rel "stylesheet"
           ! A.href "/static/css/style.css"
  H.body $ do
    H.div ! A.id "page" $ do
      H.header $ do
        H.ul ! A.id "nav" $ do
          navBar menu
          userBar user
        H.h1 (H.a ! A.href "/" $ "Le Programmeur moderne")
      H.section $ do
        H.div ! A.id "contenu" $ do
          H.h1 $ toHtml title
          body

text :: Text -> Html
text = toHtml

messageBox :: Text -> MessageType -> Handler ()
messageBox t _ = layoutPage "Message" $ text t
