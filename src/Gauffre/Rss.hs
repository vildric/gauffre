{-# LANGUAGE OverloadedStrings #-}

module Gauffre.Rss where

import Gauffre.Types

import Data.Text (Text)
import qualified Data.Text as T

-- Instances d'RssItemable

-- Fonctions génériques pour générer le flux

createFeed :: RssItemable a => Text -> Text -> [a] -> Text -> RssFeed
createFeed title descr items url = 
           RssFeed { feedItems = map toItem items
                   , feedTitle = title
                   , feedDescr = descr
                   , feedMainPage = url }

showItem :: RssItem -> Text
showItem i = "<item><title>"
  `T.append` itemTitle i
  `T.append` "</title><link>"
  `T.append` itemLink i
  `T.append` "</link><description>"
  `T.append` itemContent i
  `T.append` "</description></item>"

showFeed f = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
  `T.append` "<rss version=\"2.0\">"
  `T.append` "<channel><title>"
  `T.append` feedTitle f
  `T.append` "</title><description>"
  `T.append` feedDescr f
  `T.append` "</description><link>"
  `T.append` feedMainPage f
  `T.append` "</link>"
  `T.append` (T.concatMap showItem . feedItems) f
  `T.append` "</channel></rss>"
