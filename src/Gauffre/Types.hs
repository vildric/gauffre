{-# LANGUAGE QuasiQuotes, TemplateHaskell, TypeFamilies, OverloadedStrings #-}
{-# LANGUAGE GADTs, FlexibleContexts, GeneralizedNewtypeDeriving #-}
module Gauffre.Types where

import Data.Text
import Data.Time.Clock

import Control.Applicative
import Control.Monad
import Control.Monad.Reader
import Control.Monad.Trans.Maybe
import Control.Monad.CatchIO hiding (Handler)

import Database.Persist
import Database.Persist.GenericSql
import Database.Persist
import Database.Persist.TH

import Snap.Core

-- base de donnée

type Persistence = SqlPersist IO

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persist|
User
    name Text
    password Text
    email Text
Session
    token Text
    user UserId
    expiresAt UTCTime
Discussion
    lastUpdate UTCTime
    attachedTo String
Message
    author UserId
    discussion DiscussionId
    body Text
    postedAt UTCTime
|]

data Page = Page { pageText :: Text
                 , pageAttrs :: [(Text, Maybe Text)] }
                 deriving (Show)

-- contrôleurs

newtype Handler a = Handler { unHandler :: ReaderT HandlerState Snap a }
                   deriving (Monad,
                             Functor,
                             Applicative,
                             MonadPlus,
                             MonadIO,
                             MonadReader HandlerState,
                             Alternative,
                             MonadCatchIO,
                             MonadSnap)

data HandlerState = HandlerState { getConnection :: ConnectionPool
                                 , recaptchaConf :: RecaptchaConf
                                 , currentRoute :: Route }

data RecaptchaConf = RecaptchaConf { recaptchaPrivKey :: Text
                                   , recaptchaPubKey :: Text }

class Param a where
  toText :: a -> Text
  fromText :: Text -> Maybe a

-- Rendu

data MessageType = Notice | Failure

data RssItem = RssItem { itemTitle :: Text
                          , itemContent :: Text
                          , itemLink :: Text
                          , itemPubDate :: UTCTime }
data RssFeed = RssFeed { feedItems :: [RssItem]
                          , feedTitle :: Text
                          , feedDescr :: Text
                          , feedMainPage :: Text }

class RssItemable a where
      toItem :: a -> RssItem

-- Divers

instance (MonadSnap m) => MonadSnap (MaybeT m) where
  liftSnap = MaybeT . liftM Just . liftSnap

-- URL

data Route = Index
           | Login
           | Logout
           | Register
           | ViewPage Text
           | EditMessage MessageId


