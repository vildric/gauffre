drop table users;
create table users (
  id int primary key auto_increment,
  username varchar(32) not null,
  password varchar(64) not null,
  email varchar(64) not null
);

drop table sessions;
create table sessions (
  uid int not null,
  tok varchar(128) not null,
  expires_at datetime not null
);

drop table discussions;
create table discussions (
  id int primary key auto_increment,
  last_update datetime not null,
  attached_to varchar(64) not null
);

drop table messages;
create table messages (
  id int primary key auto_increment,
  author int not null,
  body text not null,
  posted_at datetime not null,
  discussion int not null
);
